import 'package:flutter/material.dart';
import 'package:ruiteen_app/widgets/deadlift.dart';
import 'package:ruiteen_app/widgets/shoulderpress.dart';
import 'package:ruiteen_app/widgets/squat.dart';
import 'widgets/brenchpress.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'Exercise';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('Exercise'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: const Text('Brench press'),
              onTap: () {
                setState(() {
                  body = ItemScreen1();
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Shoulder press'),
              onTap: () {
                setState(() {
                  body = ItemScreen2();
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Deadlift'),
              onTap: () {
                setState(() {
                  body = ItemScreen3();
                
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Squat'),
              onTap: () {
                setState(() {
                  body = ItemScreen4();
                });
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
