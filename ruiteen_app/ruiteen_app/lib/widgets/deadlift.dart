import 'package:flutter/material.dart';

class ItemScreen3 extends StatelessWidget {
  const ItemScreen3({Key? key}) : super(key: key);

  @override
 Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'วิธีการเล่น Rack Pulls Deadlift อย่างถูกวิธี',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                'From menfitness',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
          Icon(
            Icons.star_border_purple500,
            color: Colors.red[600],
          ),
          Text('46')
        ],
      ),
    );
    // Widget buttonSection = Container(
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     children: [
    //       _buildButtonColumn(color, Icons.call, 'CALL'),
    //       _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
    //       _buildButtonColumn(color, Icons.share, 'SHARE'),
    //     ],
    //   ),
    // );
    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'เริ่มฝึกท่า Rack Pulls ด้วยการจัดร่างกายให้อยู่ในท่าเตรียม Deadlift แล้วยกบาร์เบลขึ้นเหนือหัวเข่าเล็กน้อย จนขาทั้งสองข้างเหยียดตรง '
        'ซึ่งท่านี้จะช่วยสร้างความแข็งแรงให้กับกล้ามเนื้อหลังส่วนบนได้เป็นอย่างดี รวมถึง แขน ไหล่ และสะโพกด้วย'
        ,
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Rack Pulls Deadlift',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Rack Pulls Deadlift'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/deadlift.jpg',
              width: 400,
              height: 240,
              fit: BoxFit.contain,
            ),
            // Image.asset(
            //   'images/seated-dumbbell-shoulder-press-finish.jpg',
            //   width: 400,
            //   height: 240,
            //   fit: BoxFit.contain,
            // ),
            titleSection,
            // buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }
}


