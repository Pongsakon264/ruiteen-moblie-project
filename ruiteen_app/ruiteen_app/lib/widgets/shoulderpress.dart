import 'package:flutter/material.dart';

class ItemScreen2 extends StatelessWidget {
  const ItemScreen2({Key? key}) : super(key: key);

  @override
 Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'วิธีการเล่น Shoulder press อย่างถูกวิธี',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                'From Planforfit',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
          Icon(
            Icons.star_border_purple500,
            color: Colors.red[600],
          ),
          Text('46')
        ],
      ),
    );
    // Widget buttonSection = Container(
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     children: [
    //       _buildButtonColumn(color, Icons.call, 'CALL'),
    //       _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
    //       _buildButtonColumn(color, Icons.share, 'SHARE'),
    //     ],
    //   ),
    // );
    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'การฝึกด้วยท่า Seated Dumbbell Shoulder Press เริ่มต้นจากการ นั่งพิงเบาะ 90 องศา จับดัมเบล ยกขึ้นเหนือหัว'
        'ในลักษณะที่หันฝ่ามือออกไปทางด้านหน้าของลำตัว เหยียดแขนออกจนเกือบตึง แต่ไม่ล็อกข้อ เป็นท่าเตรียมฝึกท่า Seated Dumbbell Shoulder Press '
         
        '1.เริ่มต้นจากการค่อยๆคลายกล้ามเนื้อไหล่หน้าออก ลดดัมเบลลงจนกล้ามเนื้อไหล่หน้าถูกเหยียดตัวจนสุด ในลักษณะที่ปิดข้ออกมาข้างหน้าเล็กน้อย พร้อมกับสูดลมหายใจเข้าจนสุด '
        
        '2.ออกแรงเกร็งกล้ามเนื้อไหล่หน้า ยกดัมเบลขึ้นตรงๆ เพื่อกลับสู่ท่าเตรียม พร้อมกับปล่อยลมหายใจออกจนสุด นับเป็น 1 ครั้ง '
        '*ระหว่างการฝึกด้วยท่า Seated Barbell Shoulder Press พยายามรักษาแนวของกระดูกสันหลังให้เป็นเส้นตรงตลอดการฝึก',
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Shoulder press',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Shoulder press'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/seated-dumbbell-shoulder-press-start.jpg',
              width: 400,
              height: 240,
              fit: BoxFit.contain,
            ),
            Image.asset(
              'images/seated-dumbbell-shoulder-press-finish.jpg',
              width: 400,
              height: 240,
              fit: BoxFit.contain,
            ),
            titleSection,
            // buttonSection,
            textSection,
          ],
        ),
      ),
    );
  }
}


